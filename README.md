# WNS

```bash
wns ./examples/merge.wns -o ./tmp/styles.css --minify --reset
```

## Example files

```wns
page => page-name;

merge => ./some-file;

#foo => {
    display: flex;
    .bar => {
        display: block;
    }

    unsafe div => {
        background: red;
    }

    loose .baz => {
        text-align: center;
    }
}
```

```wns
component => some;

.foo {
    text-align: left;
}

component => other {
    .floor {
        display: grid;
    }
    .ceiling {
        display: block;
    }
}
```

## Keywords

`merge => ./relative;` - merge file `./relative.wns`, merged content is always last and can be declared only before groups and only in root scope

`page => foo;` - all selectors will starts with `#foo > `, this can be declared only in root scope

`component => foo;` - is similar to page but instead of setting leading id sets class name as leading

`component => foo {}` - creates new group with class name as identifier, this is allowed only in root scope

`unsafe selector {}` - keyword `unsafe` is required before all tag names

`loose selector {}` - keyword `loose` will prevent putting `>` between parent selector and current selector, this is valid only for class or id

`rect: width height layourt;` - special rule which is shorthand for `width`, `height` and optionally `display`

### WNS to CSS

`main.wns`

```wns
merge => landing;
merge => login;
merge => components;
```

`landing.wns`

```wns
page => landing;

component => main {
    display: flex;
    
    .content {
        display: block;
    }
}

component => sidebar {
    rect: 160px auto block;
    background: rgb(156, 100, 23);
    color: white;
}
```

`login.wns`

```wns
page => login;

component => form {
    rect: 180px auto block;
    margin: 10px auto;
}
```

`components.wns`

```wns
component => input {
    padding: 5px;
    width: 160px;
    font-size: 16px;
    color: black;
}

component => label {
    padding: 5px;
    width: 160px;
    font-size: 16px;
    color: black;
}
```

`output.css`

```css
#landing > .main {
    display: flex;
}

#landing > .main > .content {
    display: block;
}

#login > .form {
    width: 180px;
    height: auto;
    display: block;
    margin: 10px auto;
}

.input {
    padding: 5px;
    width: 160px;
    font-size: 16px;
    color: black;
}

.label {
    padding: 5px;
    width: 160px;
    font-size: 16px;
    color: black;
}
```
