#![feature(get_mut_unchecked)]

use std::fmt::Debug;
use std::iter::Peekable;
use std::rc::Rc;
use std::str::{Chars, FromStr};
use std::vec::Vec;

#[derive(Debug, PartialEq)]
enum Error {
    TokenNotFound(String),
    VarValueNotFound,
    GroupStackMalformed,
    UnexpectedEOF,
    NoRuleOwner,
    RuleValueNotFound,
    InvalidRectLayout(String),
    BadUnsafeUse,
    BadLooseUse,
    InvalidChildFile(String),
}

#[derive(Debug, PartialEq)]
struct Variable {
    name: String,
    value: String,
}

impl Variable {
    pub fn new<N, V>(name: N, value: V) -> Self
    where
        N: Into<String>,
        V: Into<String>,
    {
        Self {
            name: name.into(),
            value: value.into(),
        }
    }
}

impl Variable {
    pub fn to_css(&self, opts: &Opts) -> String {
        if opts.minify {
            format!("{}:{};", self.name, self.value)
        } else {
            format!("{}: {};", self.name, self.value)
        }
    }
}

#[derive(Debug, PartialEq)]
enum Rule {
    Css { name: String, value: String },
    Rect(RectRule),
}

impl Rule {
    pub fn to_css(&self, opts: &Opts) -> String {
        match self {
            Rule::Css { name, value } if opts.minify => format!("{}:{};", name, value),
            Rule::Css { name, value } => format!("{}: {};", name, value),
            Rule::Rect(rect) => rect.to_css(opts),
        }
    }
}

#[derive(Debug, PartialEq)]
enum RectLayout {
    Block,
}

impl FromStr for RectLayout {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "block" => Ok(RectLayout::Block),
            _ => Err(Error::InvalidRectLayout(s.to_string())),
        }
    }
}

#[derive(Debug, PartialEq)]
struct RectRule {
    width: String,
    height: String,
    layout: Option<RectLayout>,
}

impl RectRule {
    pub fn to_css(&self, opts: &Opts) -> String {
        let mut s = String::new();
        match self.layout {
            Some(RectLayout::Block) if !opts.minify => {
                s.push_str(format!("display: block; ").as_str());
            }
            Some(RectLayout::Block) if opts.minify => {
                s.push_str(format!("display:block;").as_str());
            }
            _ => (),
        };
        if !opts.minify {
            s.push_str(format!("width: {}; height: {};", self.width, self.height).as_str());
        } else {
            s.push_str(format!("width:{};height:{};", self.width, self.height).as_str());
        }
        s
    }
}

#[derive(Debug, PartialEq)]
enum GroupMarker {
    Unsafe,
    Loose,
    Strict,
}

impl Default for GroupMarker {
    fn default() -> Self {
        GroupMarker::Strict
    }
}

#[derive(Debug, PartialEq)]
enum GroupType {
    Selector,
    Pseudo,
}

impl Default for GroupType {
    fn default() -> Self {
        GroupType::Selector
    }
}

#[derive(Debug, PartialEq, Default)]
struct Group {
    name: String,
    rules: Vec<Rule>,
    groups: Vec<GroupAccess>,
    marker: GroupMarker,
    group_type: GroupType,
}

impl Group {
    pub fn new_empty<S>(name: S) -> Self
    where
        S: Into<String>,
    {
        Self {
            name: name.into(),
            ..Group::default()
        }
    }

    pub fn into_access(self) -> GroupAccess {
        Rc::new(self)
    }

    pub fn to_css(&self, path: &str, opts: &Opts) -> String {
        let Group {
            name,
            rules,
            groups,
            marker,
            group_type,
        } = self;
        let mut buffer = "".to_string();
        let p = match (path, marker, group_type) {
            ("", _, _) => name.clone(),
            (_, _, GroupType::Pseudo) => format!("{}::{}", path, name),
            (_, GroupMarker::Unsafe, _) | (_, GroupMarker::Loose, _) if opts.minify => {
                format!("{}>{}", path, name)
            }
            (_, GroupMarker::Unsafe, _) | (_, GroupMarker::Loose, _) => {
                format!("{} > {}", path, name)
            }
            (_, GroupMarker::Strict, _) if opts.minify => format!("{}>{}", path, name),
            (_, GroupMarker::Strict, _) => format!("{} > {}", path, name),
        };
        buffer.push_str(p.as_str());
        if !opts.minify {
            buffer.push(' ');
        }
        buffer.push_str("{");
        if !opts.minify {
            buffer.push('\n');
        }
        for rule in rules.iter() {
            if opts.minify {
                buffer.push_str(rule.to_css(opts).trim());
            } else {
                buffer.push_str("  ");
                buffer.push_str(rule.to_css(opts).trim());
                buffer.push('\n');
            }
        }
        buffer.push('}');
        if !opts.minify {
            buffer.push('\n');
        }
        for group in groups.iter() {
            buffer.push_str(group.to_css(&p, opts).as_str());
            if !opts.minify {
                buffer.push('\n');
            }
        }
        buffer
    }
}

type GroupAccess = Rc<Group>;

#[derive(Debug, PartialEq)]
struct File {
    path: String,
    variables: Vec<Variable>,
    groups: Vec<GroupAccess>,
    page_marker: Option<String>,
    children: Vec<Rc<File>>,
}

impl File {
    pub fn new<S>(path: S) -> Self
    where
        S: Into<String>,
    {
        Self {
            path: path.into(),
            variables: vec![],
            groups: vec![],
            page_marker: None,
            children: vec![],
        }
    }

    pub fn into_css(self, opts: &Opts) -> String {
        let File {
            path: _,
            mut variables,
            groups,
            page_marker,
            children,
        } = self;
        let path = page_marker.as_ref().cloned().unwrap_or_default();
        let mut buffer = "".to_string();

        if opts.reset_css {
            let reset = if opts.minify {
                include_str!("./reset.min.css").trim()
            } else {
                include_str!("./reset.css")
            };
            buffer.push_str(reset);
        }

        if opts.reset_css && !opts.minify {
            buffer.push_str("/*Generated*/\n");
        }

        let mut old_children = Vec::with_capacity(children.len());
        for child in children {
            let mut f = Rc::try_unwrap(child).expect("Unwrap child file failed");
            let mut old = vec![];
            std::mem::swap(&mut f.variables, &mut old);
            variables.extend(old.into_iter());
            old_children.push(f);
        }

        if !variables.is_empty() {
            buffer.push_str(":root");
            if !opts.minify {
                buffer.push(' ');
            }
            buffer.push('{');
            if !opts.minify {
                buffer.push('\n');
            }
            for var in variables.iter() {
                if !opts.minify {
                    buffer.push_str("  ");
                }
                buffer.push_str(var.to_css(opts).trim());
                if !opts.minify {
                    buffer.push('\n');
                }
            }
            buffer.push('}');
        }
        if !opts.minify {
            buffer.push('\n');
        }
        for group in groups.iter() {
            buffer.push_str(group.to_css(path.as_str(), opts).trim());
            if !opts.minify {
                buffer.push('\n');
            }
        }

        for child in old_children {
            buffer.push('\n');
            buffer.push_str(child.into_css(opts).trim());
        }

        buffer.trim().to_string()
    }
}

struct Tokenizer<'l> {
    iter: Peekable<Chars<'l>>,
    v: Vec<String>,
    buff: String,
}

impl<'l> Tokenizer<'l> {
    pub fn new(content: &'l str) -> Self {
        Self {
            iter: content.chars().peekable(),
            v: vec![],
            buff: "".to_string(),
        }
    }

    pub fn tokenize(mut self) -> Vec<String> {
        while let Some(c) = self.iter.next() {
            match c {
                ' ' | ':' | '\n' | ';' | '{' | '}' => {
                    self.drop_buffer();
                    self.push(c);
                    self.drop_buffer();
                }
                '=' if self.iter.peek() == Some(&'>') => {
                    self.iter.next();
                    self.drop_buffer();
                    self.push('=');
                    self.push('>');
                    self.drop_buffer();
                }
                _ => {
                    self.push(c);
                }
            }
        }
        self.drop_buffer();
        self.v
    }

    fn push(&mut self, c: char) {
        self.buff.push(c)
    }

    fn drop_buffer(&mut self) {
        if self.buff.is_empty() {
            return;
        }
        let mut buff = "".to_string();
        std::mem::swap(&mut self.buff, &mut buff);
        self.v.push(buff);
    }
}

trait Handler<Signal>: Parse {
    fn handle(&mut self, _: Signal) -> Result<(), Error>;

    fn check(&mut self, line: &str, _: Signal) -> bool;
}

trait Parse {
    fn parse(&mut self) -> Result<(), Error>;
    fn skip_white(&mut self);
    fn consume(&mut self) -> Option<String>;
    fn expect_consume(&mut self) -> Result<String, Error>;
    fn peek(&mut self) -> Option<&String>;
    fn consume_expected(&mut self, token: &str) -> Result<(), Error>;
    fn consume_until(&mut self, token: &str) -> Option<String>;
    fn current_group(&mut self) -> Option<&mut Group>;
}

struct Parser {
    iter: Peekable<std::vec::IntoIter<String>>,
    file: File,
    stack: Vec<GroupAccess>,
}

impl Parse for Parser {
    fn parse(&mut self) -> Result<(), Error> {
        while let Some(token) = self.peek().cloned() {
            if self.check(&token, PageMarkerSignal) {
                self.handle(PageMarkerSignal)?;
            } else if self.check(&token, MergeSignal) {
                self.handle(MergeSignal)?;
            } else if self.check(&token, ComponentSignal) {
                self.handle(ComponentSignal)?;
            } else if self.check(&token, VariableSignal) {
                self.handle(VariableSignal)?;
            } else if token.as_str() == "}" {
                break;
            } else if self.check(&token, GroupSignal) {
                self.handle(GroupSignal)?;
            } else if self.check(&token, RectRuleSignal) {
                self.handle(RectRuleSignal)?;
            } else if self.check(&token, CssSignal) {
                self.handle(CssSignal)?;
            } else if self.match_white(&token) {
                self.consume();
            } else {
                break;
            }
        }
        Ok(())
    }

    #[inline]
    fn skip_white(&mut self) {
        while let Some(s) = self.peek() {
            if !s.contains(' ') && !s.contains('\n') {
                break;
            }
            self.consume();
        }
    }

    #[inline]
    fn consume(&mut self) -> Option<String> {
        self.iter.next()
    }

    #[inline]
    fn expect_consume(&mut self) -> Result<String, Error> {
        self.consume().ok_or_else(|| Error::UnexpectedEOF)
    }

    #[inline]
    fn peek(&mut self) -> Option<&String> {
        self.iter.peek()
    }

    fn consume_expected(&mut self, token: &str) -> Result<(), Error> {
        match self.peek() {
            Some(t) if t.as_str() == token => {
                self.consume();
                Ok(())
            }
            invalid @ _ => {
                eprintln!("Unexpected token {:?}", invalid);
                Err(Error::TokenNotFound(token.to_string()))
            }
        }
    }

    fn consume_until(&mut self, token: &str) -> Option<String> {
        let mut buf = "".to_string();
        while let Some(t) = self.peek().cloned() {
            if t.as_str() == token {
                break;
            }
            self.consume();
            buf.push_str(t.as_str());
        }
        if buf.is_empty() {
            None
        } else {
            Some(buf)
        }
    }

    fn current_group(&mut self) -> Option<&mut Group> {
        self.stack
            .last_mut()
            .or(self.file.groups.last_mut())
            .and_then(|g| Rc::get_mut(g))
    }
}

impl Parser {
    pub fn new<S>(path: S, content: &str) -> Self
    where
        S: Into<String>,
    {
        let tokens = Tokenizer::new(content).tokenize();
        Self {
            iter: tokens.into_iter().peekable(),
            file: File::new(path),
            stack: vec![],
        }
    }

    pub fn parse_file(mut self) -> Result<File, Error> {
        self.parse()?;

        for group in self.stack {
            self.file.groups.push(group);
        }
        Ok(self.file)
    }

    #[inline]
    fn match_white(&mut self, line: &str) -> bool {
        line.starts_with(' ') || line.starts_with('\n')
    }
}

struct CssSignal;

impl Handler<CssSignal> for Parser {
    fn handle(&mut self, _: CssSignal) -> Result<(), Error> {
        let line = self.expect_consume()?;
        self.skip_white();
        self.consume_expected(":")?;
        self.skip_white();
        let value = self
            .consume_until(";")
            .ok_or_else(|| Error::RuleValueNotFound)?;
        self.consume_expected(";")?;
        let group = self.current_group().ok_or_else(|| Error::NoRuleOwner)?;
        group.rules.push(Rule::Css {
            name: line.to_string(),
            value,
        });
        Ok(())
    }

    #[inline]
    fn check(&mut self, line: &str, _: CssSignal) -> bool {
        !self.match_white(line)
    }
}

struct VariableSignal;

impl Handler<VariableSignal> for Parser {
    fn handle(&mut self, _: VariableSignal) -> Result<(), Error> {
        let line = self.expect_consume()?;
        self.skip_white();
        self.consume_expected(":")?;
        self.skip_white();
        let value = self
            .consume_until(";")
            .ok_or_else(|| Error::VarValueNotFound)?;
        self.consume_expected(";")?;
        self.file
            .variables
            .push(Variable::new(line.trim(), value.trim()));
        Ok(())
    }

    #[inline]
    fn check(&mut self, line: &str, _: VariableSignal) -> bool {
        line.starts_with("--")
    }
}

struct PageMarkerSignal;

impl Handler<PageMarkerSignal> for Parser {
    fn handle(&mut self, _: PageMarkerSignal) -> Result<(), Error> {
        self.expect_consume()?;
        self.skip_white();
        self.consume_expected("=>")?;
        self.skip_white();
        let name = self.expect_consume()?;
        self.skip_white();
        self.consume_expected(";")?;
        self.file.page_marker = Some(format!("#{}", name));
        Ok(())
    }

    #[inline]
    fn check(&mut self, line: &str, _: PageMarkerSignal) -> bool {
        self.stack.is_empty()
            && self.file.groups.is_empty()
            && line == "page"
            && self.file.page_marker.is_none()
    }
}

struct ComponentSignal;

impl Handler<ComponentSignal> for Parser {
    fn handle(&mut self, _: ComponentSignal) -> Result<(), Error> {
        self.expect_consume()?;
        self.skip_white();
        self.consume_expected("=>")?;
        self.skip_white();
        let name = self.expect_consume()?;
        self.skip_white();
        match self.peek().map(|s| s.as_str()) {
            Some(";") => {
                self.consume_expected(";")?;
                self.file.page_marker = Some(format!(".{}", name));
            }
            Some("{") => {
                let group = Group::new_empty(format!(".{}", name).as_str());
                self.stack.push(group.into_access());
                self.consume_expected("{")?;
                self.parse()?;
                self.consume_expected("}")?;
                let group = self.stack.pop().ok_or_else(|| Error::GroupStackMalformed)?;
                self.file.groups.push(group);
            }
            _ => self.consume_expected(";")?,
        }
        Ok(())
    }

    #[inline]
    fn check(&mut self, line: &str, _: ComponentSignal) -> bool {
        self.stack.is_empty() && line == "component"
    }
}

struct GroupSignal;

impl Handler<GroupSignal> for Parser {
    fn handle(&mut self, _: GroupSignal) -> Result<(), Error> {
        let line = self.expect_consume()?;
        let (name, marker, group_type) = match line.as_str() {
            "pseudo" => {
                self.consume_expected(" ")?;
                self.skip_white();
                let name = self.expect_consume()?;
                if name.starts_with('.') || name.starts_with('#') {
                    return Err(Error::BadUnsafeUse);
                }
                (name, GroupMarker::Strict, GroupType::Pseudo)
            }
            "unsafe" => {
                self.consume_expected(" ")?;
                self.skip_white();
                let name = self.expect_consume()?;
                if name.starts_with('.') || name.starts_with('#') {
                    return Err(Error::BadUnsafeUse);
                }
                (name, GroupMarker::Unsafe, GroupType::Selector)
            }
            "loose" => {
                self.consume_expected(" ")?;
                self.skip_white();
                let name = self.expect_consume()?;
                if !name.starts_with('.') && !name.starts_with('#') {
                    return Err(Error::BadLooseUse);
                }
                (name, GroupMarker::Loose, GroupType::Selector)
            }
            _ => (line.to_string(), GroupMarker::Strict, GroupType::Selector),
        };
        self.skip_white();
        self.consume_expected("=>")?;
        self.skip_white();
        self.consume_expected("{")?;
        let group = {
            let mut g = Group::new_empty(name);
            g.marker = marker;
            g.group_type = group_type;
            g
        }
        .into_access();
        self.stack.push(group);
        self.parse()?;
        let group = self.stack.pop().ok_or_else(|| Error::GroupStackMalformed)?;
        match self.stack.last_mut() {
            Some(stack_parent_group) => {
                if let Some(parent) = Rc::get_mut(stack_parent_group) {
                    parent.groups.push(group);
                }
            }
            None => self.file.groups.push(group),
        };
        self.skip_white();
        self.consume_expected("}")?;
        Ok(())
    }

    fn check(&mut self, line: &str, _: GroupSignal) -> bool {
        match line {
            " " | "\n" => false,
            "pseudo" | "unsafe" | "loose" => true,
            _ if line.starts_with('.') || line.starts_with('#') => true,
            _ => false,
        }
    }
}

struct RectRuleSignal;

impl Handler<RectRuleSignal> for Parser {
    fn handle(&mut self, _: RectRuleSignal) -> Result<(), Error> {
        self.expect_consume()?;
        self.skip_white();
        self.consume_expected(":")?;
        self.skip_white();
        let width = self.expect_consume()?;
        self.consume_expected(" ")?;
        self.skip_white();
        let height = self.expect_consume()?;
        self.skip_white();
        let layout = match self.peek().map(|s| s.as_str()) {
            Some(";") => {
                self.consume();
                None
            }
            None => return Err(Error::UnexpectedEOF),
            _ => {
                let layout = self
                    .expect_consume()?
                    .to_lowercase()
                    .parse::<RectLayout>()?;
                self.skip_white();
                self.consume_expected(";")?;
                Some(layout)
            }
        };

        if let Some(g) = self.current_group() {
            g.rules.push(Rule::Rect(RectRule {
                width,
                height,
                layout,
            }));
        } else {
            eprintln!("Invalid push to target");
        }
        Ok(())
    }

    fn check(&mut self, line: &str, _: RectRuleSignal) -> bool {
        line == "rect"
    }
}

struct MergeSignal;

impl Handler<MergeSignal> for Parser {
    fn handle(&mut self, _: MergeSignal) -> Result<(), Error> {
        self.expect_consume()?;
        self.consume_expected(" ")?;
        self.skip_white();
        let path = format!("{}.wns", self.expect_consume()?);
        self.skip_white();
        self.consume_expected(";")?;
        let current_file_path = std::path::PathBuf::from_str(self.file.path.as_str())
            .map_err(|e| Error::InvalidChildFile(format!("{}. {}", path.clone(), e)))?;
        let dir = current_file_path.parent().ok_or_else(|| {
            Error::InvalidChildFile(format!("No parent directory. {}", path.clone()))
        })?;
        let file_path = dir
            .join(path.as_str())
            .canonicalize()
            .map_err(|e| Error::InvalidChildFile(format!("{}. {}", path.clone(), e)))?;
        let content = std::fs::read_to_string(file_path.clone())
            .map_err(|e| Error::InvalidChildFile(format!("{}. {}", path.clone(), e)))?;
        let file = Parser::new(
            file_path
                .to_str()
                .ok_or_else(|| Error::InvalidChildFile(path.clone()))?,
            content.as_str(),
        )
        .parse_file()?;
        self.file.children.push(Rc::new(file));
        Ok(())
    }

    fn check(&mut self, line: &str, _: MergeSignal) -> bool {
        self.stack.is_empty() && self.file.groups.is_empty() && line == "merge"
    }
}

enum Output {
    File(String),
    Console,
}

struct Opts {
    output: Output,
    input: Option<String>,
    reset_css: bool,
    minify: bool,
}

impl Default for Opts {
    fn default() -> Self {
        Self {
            output: Output::Console,
            input: None,
            reset_css: false,
            minify: false,
        }
    }
}

fn main() -> Result<(), String> {
    let pwd =
        std::env::current_dir().map_err(|_| "Failed to resolve working directory".to_string())?;

    let mut args = std::env::args();
    let mut opts = Opts::default();
    while let Some(opt) = args.next() {
        match opt.as_str() {
            _ if opt.starts_with('.') || opt.starts_with('/') => {
                let path = pwd.join(opt);
                let full_path = path.display().to_string();
                opts.input = Some(full_path);
            }
            "--out" | "-o" => {
                let opt = args.next().expect("No output file");
                let path = pwd.join(opt);
                let full_path = path.display().to_string();
                opts.output = Output::File(full_path);
            }
            "--reset" => {
                opts.reset_css = true;
            }
            "--minify" => {
                opts.minify = true;
            }
            _ => (),
        }
    }
    let input = opts.input.as_ref().cloned().expect("No input file");
    let contents = std::fs::read_to_string(input.as_str())
        .map_err(|_| "Source file is not readable".to_string())?;
    let parser = Parser::new(input.as_str(), contents.as_str());

    let css = match parser.parse_file() {
        Ok(file) => file.into_css(&opts),
        Err(e) => {
            eprintln!("{:?}", e);
            return Ok(());
        }
    };
    match opts.output {
        Output::File(path) => std::fs::write(path, css.trim()).expect("Failed to write to file"),
        Output::Console => println!("{}", css.trim()),
    };
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[derive(Default)]
    struct GroupBuilder {
        name: String,
        rules: Vec<Rule>,
        groups: Vec<GroupAccess>,
        marker: GroupMarker,
        group_type: GroupType,
    }

    impl Group {
        fn build<S>(name: S) -> GroupBuilder
        where
            S: Into<String>,
        {
            GroupBuilder {
                name: name.into(),
                ..GroupBuilder::default()
            }
        }
    }

    impl GroupBuilder {
        pub fn add_rule(mut self, rule: Rule) -> Self {
            self.rules.push(rule);
            self
        }

        pub fn add_group(mut self, group: Group) -> Self {
            self.groups.push(group.into_access());
            self
        }

        pub fn set_marker(mut self, marker: GroupMarker) -> Self {
            self.marker = marker;
            self
        }

        pub fn set_type(mut self, group_type: GroupType) -> Self {
            self.group_type = group_type;
            self
        }

        pub fn build(self) -> Group {
            let GroupBuilder {
                name,
                rules,
                groups,
                marker,
                group_type,
            } = self;
            Group {
                name,
                rules,
                groups,
                marker,
                group_type,
            }
        }
    }

    struct FileBuilder {
        path: String,
        variables: Vec<Variable>,
        groups: Vec<GroupAccess>,
        page_marker: Option<String>,
        children: Vec<Rc<File>>,
    }

    impl File {
        #[inline]
        fn build<S>(path: S) -> FileBuilder
        where
            S: Into<String>,
        {
            FileBuilder {
                path: path.into(),
                variables: vec![],
                groups: vec![],
                page_marker: None,
                children: vec![],
            }
        }
    }

    impl FileBuilder {
        #[inline]
        pub fn add_var(mut self, var: Variable) -> Self {
            self.variables.push(var);
            self
        }

        #[inline]
        pub fn add_group(mut self, group: Group) -> Self {
            self.groups.push(Rc::new(group));
            self
        }

        #[inline]
        pub fn set_page<S>(mut self, name: S) -> Self
        where
            S: Into<String>,
        {
            self.page_marker = Some(name.into());
            self
        }

        #[inline]
        pub fn add_child(mut self, file: File) -> Self {
            self.children.push(Rc::new(file));
            self
        }

        #[inline]
        pub fn build(self) -> File {
            let FileBuilder {
                path,
                variables,
                groups,
                page_marker,
                children,
            } = self;
            File {
                path,
                variables,
                groups,
                page_marker,
                children,
            }
        }
    }

    #[test]
    fn tokenize_empty() {
        let code = r#""#;
        let tokens = Tokenizer::new(code).tokenize();
        let expected: Vec<String> = vec![];
        assert_eq!(tokens, expected);
    }

    #[test]
    fn tokenize_var() {
        let code = r#"--foo: 1;"#;
        let tokens = Tokenizer::new(code).tokenize();
        let expected: Vec<String> = vec![
            "--foo".to_string(),
            ":".to_string(),
            " ".to_string(),
            "1".to_string(),
            ";".to_string(),
        ];
        assert_eq!(tokens, expected);
    }

    #[test]
    fn tokenize_group() {
        let code = r#".foo=> {}"#;
        let tokens = Tokenizer::new(code).tokenize();
        let expected: Vec<String> = vec![
            ".foo".to_string(),
            "=>".to_string(),
            " ".to_string(),
            "{".to_string(),
            "}".to_string(),
        ];
        assert_eq!(tokens, expected);
    }

    #[test]
    fn tokenize_var_and_group() {
        let code = r#"--bar: 1;.foo=> {}"#;
        let tokens = Tokenizer::new(code).tokenize();
        let expected: Vec<String> = vec![
            "--bar".to_string(),
            ":".to_string(),
            " ".to_string(),
            "1".to_string(),
            ";".to_string(),
            ".foo".to_string(),
            "=>".to_string(),
            " ".to_string(),
            "{".to_string(),
            "}".to_string(),
        ];
        assert_eq!(tokens, expected);
    }

    #[test]
    fn parse_empty() {
        let code = r#""#;
        let parsed = Parser::new("", code).parse_file();
        let expected = Ok(File::new(""));
        assert_eq!(parsed, expected);
    }

    #[test]
    fn parse_var() {
        let code = r#"--foo: 1;"#;
        let parsed = Parser::new("", code).parse_file();
        let expected = Ok(File::build("").add_var(Variable::new("--foo", "1")).build());
        assert_eq!(parsed, expected);
    }

    #[test]
    fn parse_group() {
        let code = r#".foo => {}"#;
        let parsed = Parser::new("", code).parse_file();
        let expected = Ok(File::build("").add_group(Group::new_empty(".foo")).build());
        assert_eq!(parsed, expected);
    }

    #[test]
    fn parse_pseudo() {
        let code = r#".foo => { pseudo before => { display: block; } }"#;
        let parsed = Parser::new("", code).parse_file();
        let top_group = Group::build(".foo")
            .add_group(
                Group::build("before")
                    .set_type(GroupType::Pseudo)
                    .add_rule(Rule::Css {
                        name: "display".to_string(),
                        value: "block".to_string(),
                    })
                    .build(),
            )
            .build();
        let expected = Ok(File::build("").add_group(top_group).build());
        assert_eq!(parsed, expected);
        assert_eq!(
            parsed.unwrap().into_css(&Opts::default()),
            ".foo {\n}\n.foo::before {\n  display: block;\n}"
        );
    }

    #[test]
    fn parse_unsafe_group() {
        let code = r#"unsafe div => {}"#;
        let parsed = Parser::new("", code).parse_file();
        let top_group = Group::build("div").set_marker(GroupMarker::Unsafe).build();
        let expected = Ok(File::build("").add_group(top_group).build());
        assert_eq!(parsed, expected);
    }

    #[test]
    fn parse_var_and_group() {
        let code = r#"--bar: 1;.foo => {}"#;
        let parsed = Parser::new("", code).parse_file();
        let expected = Ok(File::build("")
            .add_var(Variable::new("--bar", "1"))
            .add_group(Group::new_empty(".foo"))
            .build());
        assert_eq!(parsed, expected);
    }

    #[test]
    fn parse_group_in_group() {
        let code = r#".foo => { .bar => {} }"#;
        let parsed = Parser::new("", code).parse_file();

        let mut top_group = Group::new_empty(".foo");
        top_group
            .groups
            .push(Group::new_empty(".bar").into_access());
        let expected = Ok(File::build("").add_group(top_group).build());
        assert_eq!(parsed, expected);
    }

    #[test]
    fn parse_css_rule() {
        let code = r#".foo => { display: block; }"#;
        let parsed = Parser::new("", code).parse_file();

        let mut top_group = Group::new_empty(".foo");
        top_group.rules.push(Rule::Css {
            name: "display".to_string(),
            value: "block".to_string(),
        });
        let expected = Ok(File::build("").add_group(top_group).build());
        assert_eq!(parsed, expected);
    }

    #[test]
    fn parse_multiple_css_rule() {
        let code = r#".foo => { display: block; color: red; }"#;
        let parsed = Parser::new("", code).parse_file();

        let top_group = Group::build(".foo")
            .add_rule(Rule::Css {
                name: "display".to_string(),
                value: "block".to_string(),
            })
            .add_rule(Rule::Css {
                name: "color".to_string(),
                value: "red".to_string(),
            })
            .build();
        let expected = Ok(File::build("").add_group(top_group).build());
        assert_eq!(parsed, expected);
    }

    #[test]
    fn parse_multiple_multiline_css_rule() {
        let code = r#".foo => {
        display: block; 
        color: #aef;
         }"#;
        let parsed = Parser::new("", code).parse_file();

        let top_group = Group::build(".foo")
            .add_rule(Rule::Css {
                name: "display".to_string(),
                value: "block".to_string(),
            })
            .add_rule(Rule::Css {
                name: "color".to_string(),
                value: "#aef".to_string(),
            })
            .build();
        let expected = Ok(File::build("").add_group(top_group).build());
        assert_eq!(parsed, expected);
    }

    #[test]
    fn parse_rect_without_layout() {
        let code = r#".foo => {
            rect: 12px 30px;
         }"#;
        let parsed = Parser::new("", code).parse_file();

        let top_group = Group::build(".foo")
            .add_rule(Rule::Rect(RectRule {
                width: "12px".to_string(),
                height: "30px".to_string(),
                layout: None,
            }))
            .build();
        let expected = Ok(File::build("").add_group(top_group).build());
        assert_eq!(parsed, expected);
    }

    #[test]
    fn page_test() {
        let code = r#"page => landing;.foo => { }"#;
        let parsed = Parser::new("", code).parse_file();

        let top_group = Group::new_empty(".foo");
        let expected = Ok(File::build("")
            .add_group(top_group)
            .set_page("#landing")
            .build());
        assert_eq!(parsed, expected);
        assert_eq!(
            expected.unwrap().into_css(&Opts::default()),
            "#landing > .foo {\n}".to_string()
        );
    }

    #[test]
    fn component_test() {
        let code = r#"component => landing;.foo => { }"#;
        let parsed = Parser::new("", code).parse_file();

        let top_group = Group::new_empty(".foo");
        let expected = Ok(File::build("")
            .add_group(top_group)
            .set_page(".landing")
            .build());
        assert_eq!(parsed, expected);
        assert_eq!(
            expected.unwrap().into_css(&Opts::default()),
            ".landing > .foo {\n}".to_string()
        );
    }

    #[test]
    fn component_with_block_test() {
        let code = r#"component => landing { color: green; .foo => { } }"#;
        let parsed = Parser::new("", code).parse_file();

        let top_group = Group::build(".landing")
            .add_rule(Rule::Css {
                name: "color".to_string(),
                value: "green".to_string(),
            })
            .add_group(Group::build(".foo").build())
            .build();

        let expected = Ok(File::build("").add_group(top_group).build());
        assert_eq!(parsed, expected);
        assert_eq!(
            expected.unwrap().into_css(&Opts::default()),
            ".landing {\n  color: green;\n}\n.landing > .foo {\n}".to_string()
        );
    }

    #[test]
    fn merge_test() {
        let code = include_str!("../examples/merge.wns");
        let examples_dir = std::env::current_dir()
            .unwrap()
            .join("./examples/")
            .canonicalize()
            .unwrap();
        let path = examples_dir.join("./merge.wns").canonicalize().unwrap();
        let path = path.to_str().unwrap();
        let parsed = Parser::new(path, code).parse_file();

        let child_path = examples_dir.join("./partial.wns").canonicalize().unwrap();
        let child_path = child_path.to_str().unwrap();

        let top_group = Group::build(".some")
            .add_rule(Rule::Css {
                name: "display".to_string(),
                value: "grid".to_string(),
            })
            .build();

        let expected = Ok(File::build(path)
            .add_group(top_group)
            .add_child(
                File::build(child_path)
                    .set_page("#partial-page")
                    .add_group(
                        Group::build(".bar")
                            .add_rule(Rule::Css {
                                name: "background".to_string(),
                                value: "yellow".to_string(),
                            })
                            .build(),
                    )
                    .build(),
            )
            .build());
        assert_eq!(parsed, expected);
        assert_eq!(
            expected.unwrap().into_css(&Opts::default()),
            ".some {\n  display: grid;\n}\n\n#partial-page > .bar {\n  background: yellow;\n}"
                .to_string()
        );
    }

    #[test]
    fn parse_rect_with_layout() {
        let code = r#".foo => {
            rect: 45px 60px block;
         }"#;
        let parsed = Parser::new("", code).parse_file();

        let top_group = Group::build(".foo")
            .add_rule(Rule::Rect(RectRule {
                width: "45px".to_string(),
                height: "60px".to_string(),
                layout: Some(RectLayout::Block),
            }))
            .build();
        let expected = Ok(File::build("").add_group(top_group).build());
        assert_eq!(parsed, expected);
    }

    #[test]
    fn print_rect_rule_without_layout() {
        let result = RectRule {
            width: "10px".to_string(),
            height: "20px".to_string(),
            layout: None,
        }
        .to_css(&Opts::default());
        let expected = r#"width: 10px; height: 20px;"#.to_string();
        assert_eq!(result, expected);
    }

    #[test]
    fn print_rect_rule_with_layout() {
        let result = RectRule {
            width: "10px".to_string(),
            height: "20px".to_string(),
            layout: Some(RectLayout::Block),
        }
        .to_css(&Opts::default());
        let expected = r#"display: block; width: 10px; height: 20px;"#.to_string();
        assert_eq!(result, expected);
    }
}
